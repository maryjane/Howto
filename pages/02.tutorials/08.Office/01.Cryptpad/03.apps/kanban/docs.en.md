---
title: Kanban
published: true
visible: false
indexed: true
updated:
        last_modified: "September 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - kanban
        - applications
visible: true
page-toc:
    active: false
---

# Kanban

Kanban board to organize and coordinate tasks in projects.
