---
title: Form
published: true
visible: false
indexed: true
updated:
        last_modified: "September 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - form
        - applications
visible: true
page-toc:
    active: false
---

# Form

Create and manage surveys/polls
