---
title: 'CryptPad'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - office
        - collaboration
page-toc:
  active: false
---

![](en/cryptpad_logo.svg)

# What is CryptPad?

**CryptPad** is a powerful suite of collaboration and online office tools that allows you to create, share and work with others (and in real time) on text documents, spreadsheets, presentations, polls, whiteboards, code editing and [Kanban boards](https://en.wikipedia.org/wiki/Kanban_board) for projects, in a secure and private environment. This means that the content of all your works and files is encrypted and decrypted in your web browser therefore no one (not even the server administrators) can access your information.

In many aspects, it is similar to well-known, privacy-unfriendly proprietary solutions such as **Google Docs**. The main difference is that **CryptPad** is [**free/libre software**](https://en.wikipedia.org/wiki/Free_software) and it is designed to be **private**, with **zero knowledge** from the server where it runs.

In the following chapters you will find the information needed to start using and get familiar with **CryptPad**.


# [01. Accounts](accounts)
  - Accounts types
  - Registering a new account

# [02. Users](users)
Configurations and features for

- [**Guest user**](users/guest)

- [**Registered users**](users/registered)
