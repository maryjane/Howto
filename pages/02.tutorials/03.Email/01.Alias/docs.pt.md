---
title: Como configurar um alias de email
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

Assim que tenha pedido alias de email utilizando o [formulário](https://disroot.org/forms/alias-request-form). Necessita de os configurar. Em baixo pode ler sobre como o fazer em diferentes clientes de mail.

## Table of contents
- [Webmail](webmail/)
- [Thunderbird](thunderbird/)
- [K9](k9/)
- [Mail iOS](mailios)
