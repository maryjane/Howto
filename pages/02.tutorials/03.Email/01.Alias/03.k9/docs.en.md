---
title: Email Alias: Setup on K9
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
page-toc:
    active: true
---

# Setup Alias on K9

First of, open **K9** and go to your account settings

![](en/identity_settings.png)

When in Settings, go to **"Sending Mail"** tab, tap on **"Manage Identities"**.

![](en/identity_settings2.png)

Select **"New identity"** by tapping the "three dot" icon on top right.

*(Every* **Disroot** *user has an* username@disr.it *alias to use by default)*

![](en/identity_settings3.png)

And fill in the form providing the new alias address.
![](en/identity_settings4.png)

# Set default
To change the default identity, while still in **"Manage Identities"** settings, just tap and hold the alias you want to set and select **"Move to top / make default"** option.

![](en/identity_settings5.png)

# Send email
To send email with your new alias, just tap on the **"From"** field and select alias you want to use from the dropdown menu, when composing your mail.
