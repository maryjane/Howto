---
title: Mobile client: iOS mail client
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email on iOS

1. Open up the setting of your iOS device and go to 'Mail, Contacts, Calendars'. Then select 'Add Account'.

![](en/ios_mail1.PNG)

2. Select 'Other'.

![](en/ios_mail2.PNG)

3. Select 'Add Mail Account'.

![](en/ios_mail3.PNG)

4. Insert your credentials and click 'Next'.

![](en/ios_mail4.PNG)

5. Change the hostname to disroot.org, both for incoming and outgoing mail server.

![](en/ios_mail5.PNG)

Click 'Next' and your account should be ready to use within you iOS email client.
