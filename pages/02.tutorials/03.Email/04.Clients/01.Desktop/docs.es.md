---
title: 'Correo: Clientes de escritorio'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clientes
        - escritorio
page-toc:
    active: false
---

## Clientes Multiplataforma
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)

## GNU/Linux: Integración del correo con el escritorio
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)


![](en/email_icon.png)
