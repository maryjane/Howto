---
title: Thunderbird
visible: false
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Mozilla Thunderbird

![](tb_logo.png)
**Thunderbird** is a powerful open-source email client, calendar and RSS-Feed manager. You can manage all your email accounts, calendars, news feeds, tasks and even chat from one place.

# Installing Thunderbird
If you don't have it installed yet, go to [Thunderbird page](https://www.thunderbird.net/) and choose your language and Operating System.

!! **NOTE**<br>
!! Most **GNU/Linux** distributions usually have the latest and updatable version of **Thunderbird**. We recommend using your distribution package manager to install it.

----

## [Setting up your Disroot account](setup)

## [Exporting / Importing emails](exporting)
