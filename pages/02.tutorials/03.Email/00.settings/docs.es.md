---
title: "Correo: Configuración de clientes"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - correo
        - configuraciones
page-toc:
    active: false
---

# Configuración del servidor
La mayoría de los clientes de correo electrónico detectarán la configuración correcta del servidor "automágicamente", pero en algunos casos tal vez necesites ingresar manualmente la siguiente información para configurar tu cliente:

**Servidor IMAP**: disroot.org <br>
**Puerto SSL**: 993 <br>
**Autentificación**: Contraseña Normal

**Servidor SMTP**: disroot.org <br>
**Puerto STARTTLS**: 587 <br>
**Autentificación**: Contraseña Normal

**Servidor POP**: disroot.org <br>
**Puerto SSL**: 995 <br>
**Autentificación**: Contraseña Normal

---

#### Guías relacionadas:
- [**Webmail**](/tutorials/email/webmail)
- [**Clientes de Escritorio**](/tutorials/email/clients/desktop)
- [**Clientes para Móvil**](/tutorials/email/clients/mobile)
