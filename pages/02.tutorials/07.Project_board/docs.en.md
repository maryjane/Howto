---
title: Project board
subtitle: Taiga usage tips
icon: fa-th
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - taiga
        - planning
        - project
page-toc:
    active: false
---

![](/home/icons/taiga.png)

# Taiga - Project Board

Taiga is a project management tool, developed for programmers, designers and startups working with agile methodology in mind. It can however be applied to virtually any project or group, even outside of IT realm.  It creates a clear, visual overview of the current state of your project to anyone involved. It makes planning very easy and it keeps you and your team focused on tasks. Taiga can be adjusted to fit any type of project due to its customization. From complex software development projects to simple household chores. The limitation is your imagination.<br>If you never used such tool, you will be surprised how your life can be improved with Taiga. Simply create a project, invite your group members, create tasks and put them on the board. Decide who will take responsibility for the tasks, follow progress, comment, decide and see your project flourish.
