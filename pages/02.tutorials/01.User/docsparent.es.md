---
title: Usuarixs
subtitle: Gestión de la cuenta y Datos Personales
icon: fa-user
published: true
visible: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

# Usuarixs
En esta sección puedes encontrar información útil sobre la gestión de tu cuenta.
<br>
