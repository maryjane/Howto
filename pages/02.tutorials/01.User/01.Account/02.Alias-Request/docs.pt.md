---
title: 'Como pedir um alias de email'
visible: true
published: true
taxonomy:
    category:
        - docs
---

Aliases de email estão disponíveis para apoiantes regulares. Com "apoiantes regulares" queremos dizer aqueles que nos "pagam" pelo menos um café por mês.
Não é que estejamos a promover o café, café é de facto um simbolo bastante à mão de [exploração](http://thesourcefilm.com/) [e inequidade](http://www.foodispower.org/coffee/). E pensámos que seria uma boa maneira de as pessoas terem uma medida sobre quanto querem dar.
Por favor tome o seu tempo a considerar a sua contribuição. Se apenas nos puder "pagar" um café do Rio de Janeiro por mês é ok, mas se você poder pagar um Frappuccino De Soja Duplo Descafeinado com Natas e um Shot Extra por mês, então você pode mesmo ajudar-nos a manter a platafora Disroot a funcionar e assegurar que esta se mantêm gratuita para outras pessoas com menos meios.

Encontrámos esta  [lista](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) de preços de uma chávena de café pelo mundo fora, pode não ser muito exacta, mas dá uma boa indicação dos differentes preços.

Para pedir aliases necessita de preencher este  [formulário](https://disroot.org/forms/alias-request-form).
