---
title: 'Mi cuenta'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - información
page-toc:
    active:
---

# La información de tu Cuenta

![](es/tablero_cuenta.png)

Aquí encontrarás un resumen informativo básico de la **cuenta** y **la gestión de la contraseña**, así como las **Directivas de contraseñas** (las condiciones que la contraseña debe cumplir).

![](es/cuenta.png)

Esta sección es meramente informativa.
