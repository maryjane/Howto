---
title: "Discourse: Exportar tus publicaciones en el Foro"
published: true
indexed: true
updated:
    last_modified: "Junio, 2020"		
    app: Discourse
    app_version: 2.3
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - foro
        - discourse
        - rgpd
visible: true
page-toc:
    active: false
---

**Discourse**, el software utilizado por **Disroot** para el **Foro**, te permite exportar el contenido escrito de todas tus publicaciones en un archivo de formato .csv (un archivo de texto simple que es soportado por la mayoría de los programas que manejan hojas de cálculo).

**Para exportar tus publicaciones desde Discourse:**
- Inicia sesión en el **Foro**
- Presiona tu avatar en la esquina superior derecha de la pantalla
- Presiona el botón con tu nombre de usuarix

  ![](en/export.gif)

- Presiona el botón *"Descargar mis posts"*

  ![](en/download_1.png)

- Se abrirá un cuadro de diálogo pidiendo que confirmes si quieres descargar tus publicaciones, selecciona **Sí** y luego **Ok**

  ![](en/download_2.png)

- El sistema comenzará a procesar tus datos y te notificará cuando estén listos para descargar.

![](en/download_3.png)

![](en/notification.png)

- Recibirás un mensaje del sistema notificándote que los datos están listos para ser descargados, y proporcionándote un enlace para descargar el archivo .csv con una copia de tus publicaciones. Si has habilitado las notificaciones por correo electrónico, también recibirás un correo con esta información.

- Presiona el enlace para descargar el archivo.

  ![](en/notification_2.png)  

- Este enlace estará disponible durante 48hs, tras lo cual expirará y tendrás que realizar el proceso nuevamente para exportar tus datos otra vez.

- Una vez extraído el contenido del archivo, puedes abrirlo con tu programa de hojas de cálculo.


**AVISO**: Los datos solo pueden ser descargados una vez cada 24hs.
