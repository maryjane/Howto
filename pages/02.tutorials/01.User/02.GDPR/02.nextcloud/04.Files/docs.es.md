---
title: 'Nextcloud: Exportar archivos y notas'
published: true
indexed: true
updated:
    last_modified: "Junio, 2020"
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - nube
        - archivos
        - notas
        - rgdp
visible: true
page-toc:
    active: false
---

Puedes descargar tus archivos tan fácilmente como en cualquiera del resto de las aplicaciones de **Nextcloud**.

  - Inicia sesión en la [nube](https://cloud.disroot.org)
  - Selecciona la aplicación **Archivos**
  - Selecciona todos los archivos haciendo click en el cuadro de selección
  - Luego haz click en el menú **Acciones** y selecciona *Descargar*

![](es/files_app.gif)
