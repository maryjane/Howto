---
title: Nextcloud: Fichiers & Notes
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

Vous pouvez télécharger vos fichiers et notes très facilement, comme pour toutes les applications **Nextcloud**.

1. Connectez-vous sur [cloud](https://cloud.disroot.org)

2. Choisissez l'application **Fichiers** 

![](fr/select_app.gif)

3. Sélectionnez tous les fichiers avec la case à cocher.

4. Cliquez ensuite sur **Actions** menu et choisissez *Télécharger*

![](fr/files_app.gif)
