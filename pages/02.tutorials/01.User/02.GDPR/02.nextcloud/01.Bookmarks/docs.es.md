---
title: 'Nextcloud: Exportar Marcadores'
published: true
indexed: true
updated:
    last_modified: "Junio, 2020"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - nube
        - marcadores
        - rgpd
visible: true
page-toc:
    active: false
---

Exportar los datos de tus marcadores almacenados en la nube de **Disroot** es muy sencillo.

  - Inicia sesión en la [nube](https://cloud.disroot.org).
  - Selecciona la aplicación **Marcadores**.

  ![](en/select.gif)

  - Selecciona **Configuraciones** (on the bottom of the right sidebar) y presiona el botón **Exportar**.

  ![](en/export.gif)

  - Elige dónde guardar el archivo.
