---
title: 'Nextcloud: Esportare i segnalibri'
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - bookmarks
        - gdpr
visible: true
page-toc:
    active: false
---

Esportare i tuoi link preferiti salvati nel cloud è molto semplice su **Disroot".

1. Accedi al [cloud](https://cloud.disroot.org)

2. Seleziona l'applicazione segnalibri

![](en/select_app.gif)

3. Seleziona "impostazioni" (in basso nella barra laterale) e premi il pulsante **Esporta**.

![](en/export.gif)
