---
title: Nextcloud: Exportar Calendários
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

Exportar calendários é possível muito fácilmente. Basta:

1. Fazer login na sua conta de [cloud](https://cloud.disroot.org)

2. Selecionar a aplicação Calendário


![](pt/select_app.gif)

3. Para exportar qualquer um dos seus calendários ou calendários que tenha subscrito.
Selecione o botão *"três pontinhos"* ao lado do calendário que quer exportar carregue em  *"Transferir"*. O calendário transferido será guardado num ficheiro no formato .ics.


![](pt/export_calendar.gif)

Repita o processo para todos os outros calendários que quiser exportar.
