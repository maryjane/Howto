---
title: RGPD: Nextcloud
published: true
indexed: true
updated:
    last_modified: "Junio, 2020"
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - nextcloud
        - rgpd
visible: true
page-toc:
    active: false
---
## Cómo exportar tus...

  - [**Archivos & Notas**](files)
  - [**Contactos**](contacts)
  - [**Calendarios**](calendar)
  - [**Marcadores**](bookmarks)
