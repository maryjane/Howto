---
title: Cloud
subtitle: "Basi, impostazioni, sincronizzazione, clients"
icon: fa-cloud
updated:
        last_modified: "April 2019"
        app: Nextcloud
        app_version: 15
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - cloud
page-toc:
    active: false
---

![](/home/icons/nc_logo.png)
**Nextcloud** è il servizio principale di **Disroot** e l'interfaccia utente principale che cerchiamo di integrare con la maggior parte dei servizi che offriamo. In questa guida presentare gli aspetti pricipali dell'interfaccia utente, nonché alcune azioni di base su file e impostazioni personali.
<br>

----

## Cos'è Nextlcoud?
**Nextcloud** è un software gratuito ed open source che ti consente di caricare e archiviare file su un server (potrebbe essere anche un tuo server), sincronizzandoli con diversi dispositivi a cui puoi accedere in sicurezza da qualsiasi luogo attraverso la rete Internet. Inoltre, Nextcloud offre alcune funzioni interessanti e utili come calendari, sincronizzazione di contatti e segnalibri, chiamate e videoconferenze e lettore di notizie. <br>

----------
