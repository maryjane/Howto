---
title: Android
visible: false
taxonomy:
    category:
        - docs
visible: true
page-toc:
     active: false
---

EM baixo pode aprender como integrar o nextcloud com o seu dispositivo Android.

- [Aplicação Nextcloud](nextcloud-app)
- [Sincronizar Calendários, Contactos and Tasrefas](calendars-contacts-and-tasks)
- [Migrar contactos do Google para o Nextcloud](https://howto.disroot.org/en/nextcloud/sync-with-your-cloud/android/migrating-contacts-from-google)
- [Syncing News](using-news)
- [Sincronizar Notas](Using-notes)

![](android.jpg)
