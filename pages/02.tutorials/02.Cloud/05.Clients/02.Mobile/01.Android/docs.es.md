---
title: Clientes para el móvil: Android
visible: false
taxonomy:
    category:
        - docs
visible: true
page-toc:
     active: false
---

En las siguientes guías, intentaremos ayudarte a integrar Nextcloud con tus dispositivos Android.

- [Aplicación Nextcloud](nextcloud-app)
- [Sincronizar Calendarios, Contactos y Tareas](calendars-contacts-and-tasks)
- [Migrar contactos desde Google a Nextcloud](https://howto.disroot.org/en/nextcloud/sync-with-your-cloud/android/migrating-contacts-from-google)
- [Sincronizar Noticias](using-news)
- [Sincronizar Notas](Using-notes)

![](android.jpg)
