---
title: Desktop
published: true
indexed:
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - desktop
        - clients
        - integration
visible: true
page-toc:
    active: false
---

# Desktop clients

## Multiplatform
- [**Nextcloud** client](multiplatform/desktop-sync-client)
- [**Thunderbird**](multiplatform/thunderbird-calendar-contacts)
- [**calcurse** command line client](multiplatform/calcurse-caldav)

## GNU/Linux
- [**GNOME** desktop integration](gnu-linux/gnome-desktop-integration)
- [**KDE** desktop integration](gnu-linux/kde-desktop-integration)
- [Terminal Integration](gnu-linux/terminal-integration)

## MacOS
- [**MacOS** device integration](mac-os)
