---
title: "Tareas: Web"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - tareas
        - nube
        - sincronización
page-toc:
    active: false
---

## Tareas
