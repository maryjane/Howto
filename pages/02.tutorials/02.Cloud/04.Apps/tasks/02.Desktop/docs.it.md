---
title: Tasks: Desktop
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

## Integrazione delle attività nel desktop
Puoi leggere la seguente guida per sincronizzare le **Attività** tramite un client desktop multipiattaforma.
- [Thunderbird: Calendario / Contatti / Attività sync](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

In alternativa, è possibile utilizzare e configurare l'integrazione desktop.

 - [GNOME: Integrazione desktop](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
 - [KDE: Integrazione desktop](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
