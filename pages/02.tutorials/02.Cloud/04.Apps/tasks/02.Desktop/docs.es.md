---
title: "Tareas: Escritorio"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
taxonomy:
    category:
        - docs
    tags:
        - tareas
        - nube
        - sincronización
page-toc:
    active: false
---

## Integración con el escritorio de Tareas

Puedes leer los siguientes tutoriales para tener tus **Tareas** sincronizadas utilizando clientes de escritorio multiplataforma.

- [Thunderbird: Calendarios / Contactos / Sincronización de Tareas](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

Alternativamente, puedes configurar y utilizar la integración con el escritorio.

- [GNOME: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
