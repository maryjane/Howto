---
title: Mastodon Integration
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Mastodon integration
        app_version: 0.0.11
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - mastodon
        - integration
        - dashboard
visible: true
page-toc:
    active: false
---

# Mastodon integration (coming soon)

**Mastodon integration** provides **Dashboard** widgets displaying your important notifications and your home timeline.
