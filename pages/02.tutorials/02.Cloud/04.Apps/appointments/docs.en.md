---
title: Appointments
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Appointments
        app_version: 1.7.15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - appointments
visible: true
page-toc:
    active: false
---

# Appointments (coming soon)

Book appointments into your **Disroot Calendar** via secure online form. Attendees can confirm or cancel their appointments via an email link.
