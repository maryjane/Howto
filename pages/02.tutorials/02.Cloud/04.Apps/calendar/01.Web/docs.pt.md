---
title: 'Aplicação Calendário'
published: true
visible: false
taxonomy:
    category:
        - docs
page-toc:
  active: true
---

------------
# Como usar a aplicação Calendário

Pode aceder à aplicação de calendário carregando no ícone ![](pt/calendar_top_icon.png) na barra superior da janela principal.
A *barra lateral esquerda* dá-lhe uma lista dos seus calendários, as suas opções e definições gerais.

![](pt/calendar_main.png)

--------------
## Criar um calendário
Para criar um calendário basta carregar em "**+ Novo Calendário**" na barra lateral esquerda.
Irá aparecer uma pequena janela onde pode escrever o nome que quer dar ao seu calendário e escolher uma cor para o mesmo.
Depois carregue em "**Criar**".

![](pt/calendar_add_new.png)

Pode criar vários calendários diferentes para propósitos diferentes (trabalho, activismo, etc.) repetindo este processo. Se fizer isso, pode também utilizar cores diferentes para distinguir melhor os diferentes calendários (o último ícone permite escolher qualquer cor que queira ![](pt/calendar_colorpick_icon.png)).

![](pt/calendar_list.png)

-----------------------
## Apagar, editar calendário e descarregar uma cópia.
Na **barra lateral esquerda** tem a lista dos seus calendários à direita do nome de cada calendário irá encontrar um botão de "mais opções" onde pode:

- mudar o nome ao seu calendário
- descarregar uma cópia para o seu computador
- obter um link URL para poder sincronizar o seu calendário com outros dispositivos
- apagar o seu calendário

![](pt/calendar_edit1.png)

-------------------------
## Criar um evento
Pode criar um evento no seu calendário carregando na **janela principal da aplicação** de calendários no respetivo dia do evento. Um painel irá aparecer, onde pode preencher as informações gerais do evento e carregando no botão "**Mais**", pode obter a lista completa das opções.

![](pt/calendar_edit_menu.gif)

Neste painel pode especificar:

  - título do evento
  - data de início e fim do evento
  - se é um evento de dia inteiro ou não
  - localização do evento
  - descrição do evento

![](pt/calendar_edit_menu2.png)

Se utiliza vários calendários diferentes, precisa de selecionar a qual dos seus calendários quer que este evento seja atribuido. Pode fazer isso por baixo do título do evento.

![](pt/calendar_edit_menu3.png)

Pode também fazer um lembrete carregando em "Lembrete" e "Adicionar".

![](pt/calendar_edit_menu4.png)

Pode também selecionar qual o tipo de lembrete que quer:

* áudio
* email
* popup

E escolher a antecedência do lembrete.

Basta carregar no lembrete que acabou de criar e irão aparecer as opções extra.

![](pt/calendar_edit_menu5.gif)

Também pode escolher se este é um evento que se repete ou não. Basta ver as opções em "**Repetição**".

![](pt/calendar_edit_menu6.png)

-------------------------------
## Convidar participantes para os eventos

Também pode convidar participantes para os seus eventos de calendário:

* Carregue em "Participantes"
* Preencha o campo em branco com o endereço de email da pessoa que quer convidar
* carregue em "**Adicionar**"

![](pt/calendar_edit_menu7.png)

As pessoas que convidou irão receber automaticamente um email com o convite para o evento. Quaisquer alterações que faça no evento serão enviadas por email aos participantes que convidou.

Quando tiver terminado de editar o seu evento basta carregar em "**Criar**" e o seu evento irá aparecer no ecrã.

----------------------------
## Editar ou apagar eventos
Para editar ou apagar um evento que criou no seu calendário basta carregar no evento em causa no seu ecrã. Carregue em "**Mais...**" para editar e depois em "**Actualizar**".
Para apagar um evento irá encontrar o botão grande vermelho.

![](pt/calendar_edit_menu8.gif)

----------------------------
## Partilhar calendários
Você pode partilhar os seus calendários tento com outros utilizadores do Disroot como por outras pessoas por email ou link URL.

Para partilhar com outro utilizador do Disroot:

* carregue no botão de partilha à direita do nome do seu calendário
* escreva o nome de utilizador do utilizador do Disroot com o qual quer partilhar o seu calendário
* carregue em enter

![](pt/calendar_share_menu1.png)

Para partilhar calendários via email ou link URL:

* Vá à mesma opção de "partilha"
* escollha "Share link"
* escreva o endereço de email com o qual quer partilhar o calendário
* carregue em "**Send**"
* para ter apenas o link URL público para o calendário carregue no símbolo de "corrente" ao lado do símbolo de "carta de correio"

![](pt/calendar_share_menu2.png)

------------------------------
## Importar calendários
Se tem im ficheiro de calendário ICS, vá à aplicação de calendário e carregue em  "**Settings & Import**" no canto inferior esquerdo do ecrã.

![](pt/calendar_import_menu1.png)

E selecione a opção importar calendário

![](pt/calendar_import_menu2.png)

----------------------------------------
