---
title: How-to: Mitwirken via Forum
published: false
visible: true
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - forum
        - mitwirken
page-toc:
    active: false
---


# Ein gutes Beispiel

Hier haben wir ein gutes Beispiel für die Arbeit, die noch nötig ist. Leider hat sich noch niemand bereit gefunden, ein How-to für diesen Bereich zu schreiben. Vielleicht möchtest Du dies ja zu Deinem ersten Projekt machen?!
