---
title: Como contribuir
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Contribuir

Nós pensamos que o conhecimento é uma construção coletiva. Por outras palavras, o conhecimento é o resultado de um trabalho conjunto e cooperativo em comunidade.

O Disroot, para além de custar dinheiro também consome o nosso bem mais precioso: tempo. Apesar de desde à uns meses que conseguimos cobrir os custos de manter todos os serviços a funcionar graças a donativos, documentação continua a consumir tempo.<br>
**Muito tempo precioso**

Criamos esta secção do nosso site para aqueles que queiram contribuir contribuir com o seu tempo e conhecimento para o Disroot. 

Aqui encontras informação inicial sobre diferentes formas de contribuir, desde feedback a escrever tutoriais ou traduzi-los para a tua língua.

Obrigado a todos os que apoiam e colaboram com o Disroot.

![](contribute.png)
