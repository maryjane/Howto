---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Welcome to the Disroot's How-to site.

The main goal of this site is to help you find your way around the various **Disroot** services.

We aim to cover all the services, with all its **Disroot**'s provided features, for all platforms and/or Operating Systems as well as the largest possible number of clients for the most widely used devices.

It is a very ambitious and time consuming project that requires a lot of work. But since we think it could be beneficial not only for our users (disrooters) but for all **Free Software** and **Open Source** communities running the same or similar software, we also think it is worthwhile. Because of that, any help is always needed and welcome.

So, if you think there's a missing tutorial, the information is not accurate or could be improved, please contact us and (even better) start writing a how-to yourself.

To know the different ways you can contribute, please check this [**section**](/contribute).
