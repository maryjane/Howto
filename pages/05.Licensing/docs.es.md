---
title: "Guías: Licencia"
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Licenciamiento de las guías y tutoriales
|![](en/copyleft.png)|Todo la documentación del sitio está bajo una <br><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia de Creative Commons<br>Atribución-CompartirIgual 4.0 Internacional<br>(CC BY-SA 4.0) </a><br><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png"/>|
|:--:|:--:|


Pensamos que el conocimiento debería ser compartido, por ello, hemos decidido poner todos los documentos de **Disroot Howto** a disposición para que puedan ser utilizados por otros y mejorados o adaptados a sus necesidades.<br>
También pensamos que la documentación que escribimos en estas páginas acerca de cómo utilizar nuestros servicios podría ser útil para otros proveedores de servicios basados en **Software Libre y Código Abierto** y/o a particulares que usan los mismos programas y/o comparten las mismas elecciones éticas que **Disroot**.

Al usar una licencia **Creative Commons** dejamos claro públicamente que toda la documentación (manuales, guías) publicada en [howto.disroot.org](https://howto.disroot.org/es) es libre de ser adaptada, mejorada, distribuida y/o compartida con sus modificaciones.

**Por consiguiente, solicitamos a todas aquellas personas que deseen contribuir con un nuevo a nuestra página de [Manuales](https://howto.disroot.org/es), que acepten liberarla bajo la licencia de Creative Commons BY-SA.**

La licencia **CC BY-SA**, permite a cualquiera modificar y compartir libremente un tutorial, guía o manual en tanto que las condiciones de publicación estén de acuerdo con lo siguiente:
- Atribución o reconocimiento de las adaptaciones y referencias al trabajo o autor originales.
- Compartir la versión modificada con la misma licencia (CC BY-SA), permitiendo a otros compartir y modificar el trabajo también.


Para leer más acerca del concepto de **Copyleft** puedes hacerlo [aquí](https://es.wikipedia.org/wiki/Copyleft).

Y sobre las licencias **Creative Commons** [aquí](https://creativecommons.org/).

-----
